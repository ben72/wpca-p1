	<?php include 'header.php';?>

	<section>
		<div class="container sub-features">
			<div class="row">
				<div class="col-md-4 col-sm-12 text-center single-feature">
					<a href="<?php echo $_SERVER['REQUEST_URI']; ?>about.php">
						<img class="img-responsive center-block" src="images/about-icon.png" alt="about us">
						<p>About us</p>
					</a>
				</div><!-- col -->
				<div class="col-md-4 col-sm-12 text-center single-feature">
					<a href="<?php echo $_SERVER['REQUEST_URI']; ?>schedule.php">
						<img class="img-responsive center-block" src="images/schedule-icon.png" alt="schedule a consultation">
						<p>Schedule a consultation</p>
					</a>
				</div><!-- col -->
				<div class="col-md-4 col-sm-12 text-center single-feature">
					<a href="<?php echo $_SERVER['REQUEST_URI']; ?>contact.php">
						<img class="img-responsive center-block" src="images/contact-icon.png" alt="contact us">
						<p>Contact us</p>
					</a>
				</div><!-- col -->
			</div><!-- row -->
		</div><!-- container -->
	</section>

	<?php include 'footer.php'; ?>
